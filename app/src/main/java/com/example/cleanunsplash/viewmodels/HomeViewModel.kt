package com.example.cleanunsplash.viewmodels

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.cleanunsplash.Constants
import com.example.cleanunsplash.db.RoomUnsplashDb
import com.example.cleanunsplash.models.PhotoEntity
import com.example.cleanunsplash.network.UnsplashService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import androidx.lifecycle.LiveData

class HomeViewModel(app : Application) : AndroidViewModel(app) {
    var photoList : MutableLiveData<List<PhotoEntity>> = MutableLiveData()

    private val pictureDao = RoomUnsplashDb.getInstance(getApplication()).pictureDao()


    /** If we have internet we should consume UNSPLASH API , otherwise we have to get
     * pictures from our Offline Database (ROOM)
     * **/

    /**
     * We should call our API once we have an instance of our HomeViewModel
     * **/

    init {
            if(Constants.isNetWorkAvailable(app)){
                getPicturesDetails()
            }else{
                getAllPictures()
            }
    }



    fun getPicturesList(): LiveData<List<PhotoEntity>> {
        return photoList
    }


    //Get Pictures from UNSPLASH API
    private fun getPicturesDetails(){

        val retrofit : Retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        Log.e("Internet","Connection")

        val service: UnsplashService =
            retrofit.create<UnsplashService>(UnsplashService::class.java)

        val listCall : Call<List<PhotoEntity>> = service.getRecentPhotos(
            1 , 30,"sort")


        listCall.enqueue(object : Callback<List<PhotoEntity>> {
            override fun onResponse(
                call: Call<List<PhotoEntity>>,
                response: Response<List<PhotoEntity>>
            ) {
                if(response.isSuccessful){
                    response.body()?.let { insertPictures(it) }
                    getAllPictures()
                    Log.i("Response Result","$photoList")
                }else{
                    val rc = response.code()
                    when(rc){
                        400 -> {
                            Log.e("Error 400" , "Bad Connection")
                        }
                        404 ->{
                            Log.e("Error 404" , "Not Found")
                        }
                        else ->{
                            Log.e("Error" , "$response")
                        }
                    }
                }
            }

            override fun onFailure(call: Call<List<PhotoEntity>>, t: Throwable) {
                Log.e("Error's",t!!.message.toString())
            }


        })
    }


    //Get Pictures from our Offline Database
    private fun getAllPictures() {
        val list = pictureDao.getAllPictures()
        photoList.value = list
        Log.e("ROOM","$list")
    }


    //Insert Pictures to our Offline database (ROOM) every time we consume UNSPLASH API
    private fun insertPictures(pictures: List<PhotoEntity>){
        val ids = pictureDao.insertPictures(pictures)
        Log.e("id","$ids")
    }



    //Filter List
    fun filterList(filterItem: String) : MutableList<PhotoEntity> {
        var templeList : MutableList<PhotoEntity> = ArrayList()
        for(d in photoList.value!!){
            if(filterItem in d.user.username.toString()){
                templeList.add(d)
            }
        }

        return templeList
    }

    //updateStatusStory In ROOM
    fun updateStoryStatus(image: PhotoEntity){
        pictureDao.updatePicture(image)
    }

}