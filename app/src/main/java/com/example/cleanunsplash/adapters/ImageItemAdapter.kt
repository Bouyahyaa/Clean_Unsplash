package com.example.cleanunsplash.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.cleanunsplash.databinding.ImageItemBinding
import com.example.cleanunsplash.models.PhotoEntity

open class ImageItemAdapter(
    private val context: Context,
    private var list: ArrayList<PhotoEntity>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var onClickListener : OnClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return MyViewHolder(
            ImageItemBinding.inflate(LayoutInflater.from(context),parent,
                false)
        )
    }

    fun setOnClickListener(onClickListener: OnClickListener){
        this.onClickListener = onClickListener
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val model = list[position]

        if (holder is MyViewHolder) {

            holder.binding.title.text = model.user.username
            holder.binding.desc.text = model.created_at


            Glide.with(context)
                .load(model.urls.regular)
                .placeholder(ColorDrawable(Color.parseColor(model.color)))
                .into(holder.binding.image)

            holder.itemView.setOnClickListener{
                if(onClickListener != null){
                    onClickListener!!.onClick(position,model)
                }
            }
            

            holder.binding.image.setOnClickListener{
                if(onClickListener != null){
                    onClickListener!!.onClick(position,model)
                }
            }
        
        }
    }


    override fun getItemCount(): Int {
        return list.size
    }


    @SuppressLint("NotifyDataSetChanged")
    fun updateList(photos: List<PhotoEntity>){
        list = photos as ArrayList<PhotoEntity>
        notifyDataSetChanged()
    }


    interface OnClickListener{
        fun onClick(position: Int, model: PhotoEntity)
    }

    private class MyViewHolder(binding: ImageItemBinding) : RecyclerView.ViewHolder(binding.root){
        var binding = binding
    }
}
