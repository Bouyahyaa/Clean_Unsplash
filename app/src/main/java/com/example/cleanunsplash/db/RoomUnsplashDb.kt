package com.example.cleanunsplash.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.cleanunsplash.models.PhotoEntity

@Database(entities = [PhotoEntity::class],version = 13, exportSchema = false)
abstract class RoomUnsplashDb:RoomDatabase(){
    abstract fun pictureDao():PictureDao


    companion object {
        @Volatile
        private var INSTANCE: RoomUnsplashDb? = null
        fun getInstance(context: Context): RoomUnsplashDb {
            synchronized(this) {
                var instance = INSTANCE
                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        RoomUnsplashDb::class.java,
                        "unsplash_database"
                    )
                        .fallbackToDestructiveMigration()
                        .allowMainThreadQueries()
                        .build()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}