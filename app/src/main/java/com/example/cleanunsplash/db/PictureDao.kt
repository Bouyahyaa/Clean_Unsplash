package com.example.cleanunsplash.db

import androidx.room.*
import com.example.cleanunsplash.models.PhotoEntity

@Dao
interface PictureDao {

    @Query("SELECT * FROM pictures")
    fun getAllPictures() : List<PhotoEntity>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertPictures(pictures: List<PhotoEntity>) : List<Long>


    @Update
    fun updatePicture(image: PhotoEntity): Int

}