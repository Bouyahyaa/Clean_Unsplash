package com.example.cleanunsplash.network

import com.example.cleanunsplash.Constants
import com.example.cleanunsplash.models.PhotoEntity
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface UnsplashService {
    @GET("photos/?client_id=${Constants.APP_ID}")
    fun getRecentPhotos(
        @Query("page") page: Int,
        @Query("per_page") pageLimit: Int,
        @Query("order_by") order: String
    ) : Call<List<PhotoEntity>>
}