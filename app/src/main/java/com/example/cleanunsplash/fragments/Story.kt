package com.example.cleanunsplash.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.cleanunsplash.R
import com.example.cleanunsplash.databinding.FragmentStoryBinding
import jp.shts.android.storiesprogressview.StoriesProgressView

class Story : Fragment() {

    private lateinit var binding: FragmentStoryBinding
    private val args by navArgs<StoryArgs>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentStoryBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val myStory = args.currentStory
        binding.stories.setStoriesCount(1)
        binding.stories.setStoryDuration(1500L)
        binding.stories.startStories()
        binding.stories.setStoriesListener(object : StoriesProgressView.StoriesListener{
            override fun onNext() {
                Log.e("Next","Story")
            }


            override fun onComplete() {
                Navigation.findNavController(view)
                    .navigate(R.id.navigateStoryToHome)
            }

        })


        binding.image.setOnClickListener{
            binding.stories.skip()
        }

        this.context?.let {
            Glide.with(it)
                .load(myStory.urls.regular)
                .into(binding.image)
        }

    }


}