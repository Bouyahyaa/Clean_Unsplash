package com.example.cleanunsplash.fragments

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.cleanunsplash.R
import com.example.cleanunsplash.adapters.ImageItemAdapter
import com.example.cleanunsplash.adapters.StoryItemAdapter
import com.example.cleanunsplash.databinding.FragmentHomeBinding
import com.example.cleanunsplash.models.PhotoEntity
import com.example.cleanunsplash.viewmodels.HomeViewModel
import com.google.gson.Gson

class Home : Fragment() {
    private val homeViewModel: HomeViewModel by viewModels()
    private lateinit var binding: FragmentHomeBinding
    private lateinit var picturesAdapter : ImageItemAdapter
    private lateinit var storiesAdapter : StoryItemAdapter
    private var mProgressDialog : Dialog? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater,container,false)
        Log.e("onCreateView","Called")
        return binding.root
    }


    @SuppressLint("UseRequireInsteadOfGet")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.e("onViewCreated","Called")

        // Circular Progress shown until we get The Data
        showCustomProgressDialog()

        homeViewModel.getPicturesList().observe(viewLifecycleOwner,  { it ->
            if(!it.isNullOrEmpty()){
                //Hide circular progress
                hideProgressDialog()
                val picturesList : List<PhotoEntity> = Gson()
                    .fromJson(Gson().toJson(it),Array<PhotoEntity>::class.java)
                    .toList()
                Log.i("List","$picturesList")

                //Setup UI For Principal Pictures (home)
                binding.rvPictures.layoutManager = GridLayoutManager(this.context,2)
                binding.rvPictures.setHasFixedSize(true)
                picturesAdapter = this.context?.let { ImageItemAdapter(it,picturesList.toCollection(ArrayList<PhotoEntity>())) }!!
                binding.rvPictures.adapter = picturesAdapter

                picturesAdapter.setOnClickListener(object : ImageItemAdapter.OnClickListener {
                    override fun onClick(position: Int, model: PhotoEntity) {
                        val action = HomeDirections.navigateHomeToPictureDetail(model)
                        Navigation.findNavController(view)
                            .navigate(action)
                    }

                })


                //Setup UI For Stories Pictures
                binding.rvStories.layoutManager = LinearLayoutManager(this.context,LinearLayoutManager.HORIZONTAL,false)

                storiesAdapter = StoryItemAdapter(this.context!!, it as ArrayList<PhotoEntity>)

                binding.rvStories.adapter = storiesAdapter

                storiesAdapter.setOnClickListener(object : StoryItemAdapter.OnClickListener{
                    @SuppressLint("NotifyDataSetChanged")
                    override fun onClick(position: Int, model: PhotoEntity) {
                        val action = HomeDirections.navigateHomeToStory(model)
                        Navigation.findNavController(view)
                            .navigate(action)
                        model.Clicked = true
                        homeViewModel.updateStoryStatus(model)
                        storiesAdapter.notifyDataSetChanged()
                    }

                })

            }
        })


        //Filter List
        binding.mySearchView.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(
                s: CharSequence?,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence?,
                start: Int,
                before: Int,
                count: Int
            ) {
            }

            override fun afterTextChanged(s: Editable?) {
                val photos = homeViewModel.filterList(s.toString())
                Log.e("photos","$photos")
                picturesAdapter.updateList(photos)
            }
        })
    }


    private fun hideProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog!!.dismiss()
        }
    }

    private fun showCustomProgressDialog() {
        mProgressDialog = this.context?.let { Dialog(it) }
        mProgressDialog!!.setContentView(R.layout.dialog_custom_progress)
        mProgressDialog!!.show()
    }

}