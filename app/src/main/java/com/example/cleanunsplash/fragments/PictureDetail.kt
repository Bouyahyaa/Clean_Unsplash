package com.example.cleanunsplash.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.cleanunsplash.R
import com.example.cleanunsplash.databinding.FragmentPictureDetailBinding


class PictureDetail : Fragment() {
    private lateinit var binding: FragmentPictureDetailBinding
    private val args by navArgs<PictureDetailArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPictureDetailBinding.inflate(inflater, container, false)
        return binding.root
    }


    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val picture = args.currentPicture
        binding.name.text = picture.user.username
        binding.descriptionPopup.text = picture.description
        binding.createdAt.text = "Created at : " + picture.created_at
        binding.updatedAt.text = "Updated at : " + picture.updated_at

        this.context?.let {
            Glide.with(it)
                .load(picture.urls.regular)
                .into(binding.photo)
        }

        this.context?.let {
            Glide.with(it)
                .load(picture.user.profile_image.large)
                .into(binding.profileImage)
        }


        binding.returnButton.setOnClickListener {
            Navigation.findNavController(view)
                .navigate(R.id.navigatePictureDetailToHome)
        }

    }

}